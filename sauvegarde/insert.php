<?php 
function echocsv($fields)
{
    $separator = '';
    foreach ($fields as $field) {
        if (preg_match('/\\r|\\n|,|/', $field)) {
            $field = '' . str_replace('', '', $field) . '';
        }
        echo $separator . $field;
        $separator = ';';
    }
    echo "\r";
}

// gestion des erreurs
error_reporting(E_ALL ^ E_DEPRECATED);

//connexion � la base  
$link = mysqli_connect("localhost","root","","testaa");
//verification de la connexion 
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//requ�te a effectuer


mysqli_query($link,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

mysqli_query($link, "LOAD DATA LOCAL INFILE 'ATraiter.txt' REPLACE INTO TABLE insertion FIELDS TERMINATED BY ';' LINES TERMINATED BY '\r'  ;");




/*
* PHP code to export MySQL data to CSV

*
* Sends the result of a MySQL query as a CSV file for download
*/
/*
* establish database connection
*/

/*
* execute sql query
*/
$query = sprintf('SELECT * FROM insertion');
$result = mysqli_query($link,"SELECT * FROM insertion;");
/*
* send response headers to the browser
* following headers instruct the browser to treat the data as a csv file called export.csv
*/
header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename=export.csv');
/*
* output header row (if atleast one row exists)
*/
$row = mysqli_fetch_assoc($result);
if ($row) {
    echocsv(array_keys($row));
}
/*
* output data rows (if atleast one row exists)
*/
while ($row) {
    echocsv($row);
    $row = mysqli_fetch_assoc($result);
}
/*
* echo the input array as csv data maintaining consistency with most CSV implementations
* - uses double-quotes as enclosure when necessary
* - uses double double-quotes to escape double-quotes 
* - uses CRLF as a line separator
*/
mysqli_close($link);
?>
