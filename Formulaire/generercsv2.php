<?php
function echocsv($fields)
{
    $separator = '';
    foreach ($fields as $field) {
        if (preg_match('/\\r|\\n|,|/', $field)) {
            $field = '' . str_replace('', '', $field) . '';
        }
        echo $separator . $field;
        $separator = ';';
    }
    echo "\r";
}

// gestion des erreurs
ini_set("display_errors",0);error_reporting(0);
error_reporting(E_ALL ^ E_DEPRECATED);

//connexion à la base  
$link = mysqli_connect("172.16.99.3","g.bertrand","p@sse12345","g.bertrand");

//verification de la connexion 
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//requêtes a effectuer
mysqli_query($link,"SET head off,character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

/*
* PHP code to export MySQL data to CSV
* Sends the result of a MySQL query as a CSV file for download
*/
/*
* establish database connection
*/
/*
* execute sql query
*/

//IdOrigine, IdTableOrigine, IdRubOrigine,NomDoc, TypeLouD, CheminAcces
//SELECT IdOrigine as a, IdTableOrigine as z, IdRubOrigine as e,NomDoc as r, TypeLouD as t, CheminAcces as y FROM autretable;

//$query = sprintf('SELECT * FROM autretable;');
$result = mysqli_query($link,"SELECT * FROM autretable;");
/*
* send response headers to the browser
* following headers instruct the browser to treat the data as a csv file called export.csv
*/
header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename=06_Lien_IMAGES_et_DOCUMENTS_AVENIO_PROD.csv');
//header_remove();
/*
* output header row (if atleast one row exists)
*/

$row = mysqli_fetch_assoc($result);
$row = str_replace('\n', '', $row);
// remplacer saut de ligne avec str_replace
// substr 2 premier caractère pour enlever \n

if ($row) {
    echocsv(array_keys($row));
}
/*
* output data rows (if atleast one row exists)
*/
while ($row) {
    echocsv($row);
    $row = mysqli_fetch_assoc($result);
}
/*
* echo the input array as csv data maintaining consistency with most CSV implementations
* - uses double-quotes as enclosure when necessary
* - uses double double-quotes to escape double-quotes 
* - uses CRLF as a line separator
*/

//Purge des trois tables

//mysqli_query($link,"DELETE FROM insertion;");
//mysqli_query($link,"DELETE FROM tablefinale;");
//mysqli_query($link,"DELETE FROM autretable;");

mysqli_close($link);
//ob_start(); 
?>
